import React from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import PersonAdd from 'material-ui/svg-icons/social/person-add';
import ContentLink from 'material-ui/svg-icons/content/link';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';
import ContentCopy from 'material-ui/svg-icons/content/content-copy';

const headerMenu = [
    {
        title: 'Одежда',
        icon: <RemoveRedEye/>,
        rightIcon: <ArrowDropRight />,
        key: 1,
        menuItems: [
            <MenuItem primaryText="Мужская "/>,
            <MenuItem primaryText="Женская"/>,
            <MenuItem primaryText="Детская"/>,
            <MenuItem primaryText="Другое"/>,
        ]
    },
    {
        title: 'Телефон',
        icon: <PersonAdd/>,
        key: 2
    },
    {
        title: 'Контакты',
        icon: <ContentLink/>,
        key: 3
    },
    {
        title: 'Регистрация',
        icon: <ContentCopy/>,
        key: 4
    },
];

export const ArgentHeader = (props) => {
    const leftElement = !props.showTitle ?
        <IconButton onClick={props.toggleTitle}><NavigationMenu/></IconButton> :
        <IconButton onClick={props.toggleTitle}><NavigationClose/></IconButton>;
    return (
        <div>
            <Drawer open={props.showTitle}
                    containerStyle={{
                        marginTop: '65px',
                    }}>
                {headerMenu.map((element) => {
                    return <MenuItem key={element.key}
                                     leftIcon={element.icon}
                                     rightIcon={element.rightIcon}
                                     menuItems={Boolean(element.menuItems) && element.menuItems}
                    >{element.title}</MenuItem>
                })}
            </Drawer>
            <AppBar
                style={{backgroundColor: '#fffaf1'}}
                title={<span>Магазин одежды</span>}
                iconElementLeft={leftElement}
                iconElementRight={<FlatButton label="Зарегистрироваться"/>}
            />
        </div>
    )
};