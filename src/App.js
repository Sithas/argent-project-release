import React, {Component} from 'react';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {ArgentHeader} from './ArgentHeader'
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';

import './App.css';
import {ArgentItem} from "./ArgentItem";

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
    },
    gridList: {
        width: 500,
        height: 450,
        overflowY: 'auto',
    },
};

const tilesData = [
    {
        img: './item.jpg',
        key: 1,
        title: 'Breakfast',
        author: 'jill111',
    },
    {
        img: './item.jpg',
        key: 2,
        title: 'Tasty burger',
        author: 'pashminu',
    },
    {
        img: './item.jpg',
        key: 3,
        title: 'Camera',
        author: 'Danson67',
    },
    {
        img: './item.jpg',
        key: 4,
        title: 'Morning',
        author: 'fancycrave1',
    },
    {
        img: './item.jpg',
        key: 5,
        title: 'Hats',
        author: 'Hans',
    },
    {
        img: './item.jpg',
        key: 6,
        title: 'Honey',
        author: 'fancycravel',
    },
    {
        img: './item.jpg',
        key: 7,
        title: 'Vegetables',
        author: 'jill111',
    },
    {
        img: './item.jpg',
        key: 8,
        title: 'Water plant',
        author: 'BkrmadtyaKarki',
    },
];

class App extends Component {
    constructor() {
        super();
        this.state = {
            showTitle: true
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = () => {
        const newState = !this.state.showTitle;
        this.setState({showTitle: newState})
    };

    render() {
        return (
            <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                <div>
                    <ArgentHeader showTitle={this.state.showTitle} toggleTitle={this.handleChange}/>
                    <div style={{margin: '0 auto', width: '500px'}}>
                        <Paper style={{
                            minWidth: '800px',
                            minHeight: '300px',
                            marginTop: '30px',
                            padding: '25px'
                        }} zDepth={3}>
                            {tilesData.map((element) => {
                            return <ArgentItem data={element} />
                        })}
                        </Paper>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default App;
