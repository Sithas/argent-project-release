import React from 'react'

export class ArgentItem extends React.Component {
    render() {
        const style = {
            borderRadius: '3px',
            display: 'inline-block',
            width: '200px',
            minHeight: '250px',
            margin: '5px 10px 8px 5px',
            padding: '8px',
            backgroundImage: `url("${this.props.data.img ? this.props.data.img : './item.jpg'}")`,
            backgroundSize: '100%',
            backgroundRepeat: 'no-repeat',
            boxShadow: 'rgba(0, 0, 0, 0.19) 0px 10px 30px, rgba(0, 0, 0, 0.23) 0px 6px 10px'
        };
        const textstyle = {
            color: '#eeeeee',
            fontSize: '14px',

        };
        const iteminfostyle = {
            marginTop: '210px',
        };
        const popoverstyle = {
            position: 'absolute',
            marginTop: '200px',
            background: 'black',
            minWidth: '200px',
            padding: '20px 10px',
            transform: 'translateX(-10px)',
            textAlign: 'center',
            fontSize: '18px',
            display: 'none',
        };
        return (
            <div key={this.props.data.key} style={style}>
                <div style={popoverstyle}>БЫСТРЫЙ ПРОСМОТР</div>
                <div style={iteminfostyle}>1234</div>
            </div>
        )
    }
}
